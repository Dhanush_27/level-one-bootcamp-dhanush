//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>
int main()
{
    int n,sum=0,i,val;
    printf("Enter the number of integers for addition : ");
    scanf("%d",&n);
    printf("Enter %d numbers \n",n);
    for(i=0;i<n;i++)
    {
        scanf("%d",&val);
        sum=sum+val;
    }
    printf("The sum of %d is %d",n,sum);
    return 0;
}
