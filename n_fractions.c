//WAP to find the sum of n fractions.
#include<stdio.h>
struct fraction
{
    int a,b;
};
typedef struct fraction frac;
frac input_fraction()
{
    frac fract;
    printf("Enter the numerator : \n");
    scanf("%d",& fract.a);
    printf("Enter the denominator : \n");
    scanf("%d",&fract.b);
    return fract;
}

void input(int n, frac a[])
{
    for(int i =0;i<n;i++)
    {
        a[i]=input_fraction();
    }
}
int gcd(int num, int den)
{
    int div;
    for(int i=1;i<=num && i<=den;i++)
    {
        if(num%i==0 && den%i==0)
        {
            div=i;
            
                  
        }
    }
    
    return div;
}
frac compute_sum(int n,frac a[])
{
    frac sum;
    sum.a=0;
    sum.b=1;
    for(int i=0;i<n;i++)
    {
        sum.a=(a[i].a)*(sum.b)+(sum.a)*(a[i].b);
        sum.b=(sum.b)*(a[i].b);
    }
    int divisor=gcd(sum.a,sum.b);
    sum.a=sum.a/divisor;
    sum.b=sum.b/divisor;
    return sum;
}

void display_output(int n,frac a[],frac result)
{
    for(int i=0;i<n;i++)
    {
        if(i<n-1)
        {
            printf("%d / %d + ",a[i].a,a[i].b);
        }
        else 
        {
            printf("%d / %d = %d / %d",a[i].a,a[i].b,result.a,result.b);
        }
    }
}
        
int main()
{
    int n;
    printf("Enter the number of fraction :");
    scanf("%d",&n);
    frac a[n];
    input (n,a);
    frac result=compute_sum(n,a);
    display_output(n,a,result);
}
