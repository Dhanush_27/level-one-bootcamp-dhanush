//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>
int input()
{
    int n;
    printf("Enter the number to be added : ");
    scanf("%d",&n);
    return n;
}
int enter_sum(int a, int b)
{
    int sum;
    sum = a+b;
    return sum;
}
void output(int a,int b,int c)
{
    printf("The sum of %d and %d is %d ",a,b,c);
}
int main()
{
    int x,y,z;
    x=input();
    y=input();
    z=enter_sum(x,y);
    output(x,y,z);
return 0;
}

