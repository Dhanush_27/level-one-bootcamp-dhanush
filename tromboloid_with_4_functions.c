//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>
int input()
{
    int n; 
    printf("Enter the number for height,diameter and base respectively\n");
    scanf("%d",&n);
    return n;
}

int volume(int h, int d ,int b)
{
    int vol;
    vol = 0.3333333333*((h*d*b)+(d/b));
    return vol;
}

void output(int vol)
{
    printf("The volume is %d",vol);
}

int main()
{
    int w,x,y,z;
    w=input();
    x=input();
    y=input();
    z=volume(w,x,y);
    output(z);
    return 0;
}
