//WAP to find the sum of two fractions.
#include<stdio.h>
struct fraction
{
    int a,b;
};
struct fraction sum(struct fraction fr1 , struct fraction fr2);
void display(struct fraction fr1, struct fraction fr2, struct fraction fract);
int gcd(int num, int den)
{
    int div;
    if(num>den)
    {
        div = den;
    }
    else
    {
        div = num;
    }
    int g;
    for(int i=div;i>0;i--)
    {
        if(num%i ==0 && den%i ==0)
        {
            g=i;
            break;
        }
    }
    return 0;
}

struct fraction input()
{
    struct fraction fract;
    printf("Enter the numerator : \n");
    scanf("%d",& fract.a);
    printf("Enter the denominator : \n");
    scanf("%d",&fract.b);
    return fract;
}

struct fraction sum(struct fraction fr1 , struct fraction fr2)
{
    struct fraction frsum;
    frsum.a = (fr1.a * fr2.b) + (fr2.a * fr1.b);
    frsum.b = (fr1.b * fr2.b);
    int g;
    g = gcd(frsum.a , frsum.b);
    frsum.a = frsum.a / g;
    frsum.b = frsum.b / g; 
    return frsum;
}
void display(struct fraction fract1, struct fraction fract2, struct fraction fract)
{
    printf("The sum of %d %d and %d %d is %d %d",fract1.a ,fract1.b, fract2.a ,fract2.b,fract.a,fract.b);
}
int main()
{
    struct fraction fract1, fract2, fract3;
    fract1= input();
    fract2=input();
    fract3=sum(fract1,fract2);
    display (fract1, fract2, fract3);
    return 0;
}
